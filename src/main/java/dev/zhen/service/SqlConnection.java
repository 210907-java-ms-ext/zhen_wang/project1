package dev.zhen.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqlConnection {
    private Connection connection;
    public Connection establishConnection() throws SQLException {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        //jdbc:postgresql://localhost:5432/postgres"
        String url = System.getenv("posturl");
        String username = System.getenv("postuser");
        String password = System.getenv("postpass");
        connection = DriverManager.getConnection(url, username,password);
        return connection;
    }
}
