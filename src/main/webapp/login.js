document.getElementById("submit").addEventListener("click", attemptLogin);

function attemptLogin(){
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;

    console.log(`username: ${username}, password ${password}`)

    const xhr = new XMLHttpRequest();
    xhr.open("POST", "./log");
     xhr.onreadystatechange = function(){
       if(xhr.readyState===4){

        if(xhr.status===401){
            let header = document.getElementById("txt");
            header.innerText = "Invalid Log In credentials"

         } else if (xhr.status===200){
              const token = xhr.getResponseHeader("Authorization");
              console.log(token);
              sessionStorage.setItem("token", token);
                var path = token.split(":")
                if(path[1] ==="admin")
                {
                window.location.href="./admin.html";
                }
                if(path[1]==="user")
                {
                window.location.href="./user.html"}
        }
         else{
                 let header = document.getElementById("txt");
                 header.innerText = "Error  ";
     }
     }
    }
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        const requestBody = `username=${username}&password=${password}`;
        xhr.send(requestBody);

}
