package dev.zhen.model;

import java.util.Objects;

public class Reimbursement {
    private User user;
    private int id;
    private String name;
    private double value;
    private int Complete;

    public Reimbursement()
    {
        super();
    }
    public Reimbursement(User user, int id, String name, double value, int Complete)
    {
        this.user = user;
        this.id = id;
        this.name = name;
        this.value = value;
        this.Complete = Complete;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getComplete() {
        return Complete;
    }

    public void setComplete(int complete) {
        Complete = complete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reimbursement that = (Reimbursement) o;
        return id == that.id && Double.compare(that.value, value) == 0 && Complete == that.Complete && Objects.equals(user, that.user) && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, id, name, value, Complete);
    }

    @Override
    public String toString() {
        return "Reimbursement{" +
                "user=" + user +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", value=" + value +
                ", Complete=" + Complete +
                '}';
    }
}
