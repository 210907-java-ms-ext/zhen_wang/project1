package dev.zhen.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.zhen.model.User;
import dev.zhen.service.AuthorizationService;
import dev.zhen.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class UserServlet extends HttpServlet {

    AuthorizationService authorizationService = new AuthorizationService();
    UserService userService = new UserService();

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String token = request.getHeader("Authorization");

        boolean isValidToken = authorizationService.validation(token);
        if (!isValidToken) {
            response.sendError(400, "Bad Token");
        } else {
            User currentUser = authorizationService.findUserById(token);
            if (currentUser.getId() == 0) {
                response.sendError(401, "no user");
            } else {
                if (currentUser.getRole().equals("admin")) {
                    try (PrintWriter pw = response.getWriter()) {
                        ArrayList<User> users = userService.getAllUser();
                        ObjectMapper om = new ObjectMapper();
                        String userJson = om.writeValueAsString(users);
                        pw.write(userJson);
                    }

                }else if (currentUser.getRole().equals("user"))
                {   String tokenArr[] = token.split(":");
                    int id = Integer.parseInt(tokenArr[0]);
                   try(PrintWriter pw = response.getWriter()) {

                   User users = userService.getUserById(id);
                   ObjectMapper om = new ObjectMapper();
                   String userJson = om.writeValueAsString(users);
                   pw.write(userJson);
                }}else
                {
                    response.sendError(403, "Wrong user");
                    System.out.println("Wrong user");
                }

            }
        }
    }
}

