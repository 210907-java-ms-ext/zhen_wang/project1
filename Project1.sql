
create table project1(
	user_id serial primary key,
	first_name varchar(50) not null,
	last_name varchar(50) not null,
	user_name varchar(50) not null,
	user_password varchar(50) not null,
	user_email varchar(50) not null,
	user_role varchar(50) not null
);

insert into project1 (first_name, last_name, user_name, user_password, user_email, user_role) values ('Zhen', 'Wang', 'Zhen', 'password', 'zhen@email.com', 'admin');
insert into project1 (first_name, last_name, user_name, user_password, user_email, user_role) values ('Bob', 'Test', 'bob', 'cool', 'bob@email.com', 'user');
insert into project1 (first_name, last_name, user_name, user_password, user_email, user_role) values ('Sam', 'Son', 'Sam', 'Son', 'Samson@email.com', 'user');

create table reimbursement(
	reimburst_id serial primary key,
	user_id int references project1,
	reimburst_name varchar(50) not null,
	value decimal(7,2),
	completed int
);


insert into reimbursement (user_id, reimburst_name, value, completed) values (1,'zhenwang',23.00, 3);
insert into reimbursement (user_id, reimburst_name, value, completed) values (2,'BobTest',15.99, 1);
insert into reimbursement (user_id, reimburst_name, value, completed) values (2,'BobTest',5.00, 1);
insert into reimbursement (user_id, reimburst_name, value, completed) values (2,'BobTest',10.00, 2);
insert into reimbursement (user_id, reimburst_name, value, completed) values (2,'BobTest',100.00, 1);
insert into reimbursement (user_id, reimburst_name, value, completed) values (3,'SamSon',40.00, 3);

