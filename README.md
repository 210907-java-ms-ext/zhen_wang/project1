# PROJECT NAME <br />
Expense Reimbursement System - Java

# Project Description <br />
The Expense Reimbursement System (ERS) will manage the process of reimbursing employees for expenses incurred while on company time. All employees in the company can login and submit requests for reimbursement and view their past tickets and pending requests. Finance managers can log in and view all reimbursement requests and past history for all employees in the company. Finance managers are authorized to approve and deny requests for expense reimbursement.

# Technologies Used <br />
Servlets, Java, JavaScript, HTML, CSS, JDBC, SQL, AJAX, Bootstrap, RDS, Tomcat, Git, Maven
# Getting started <br />
git clone https://gitlab.com/210907-java-ms-ext/zhen_wang/project1.git <br />
in AWS EC2 install Maven, Tomcat, and Java 8 <br />
set environment varaibles for database URL(posturl), username(postuser), and password(postpass) <br />
build using Maven <br />
copy the war file into Tomcat <br />
start tomcat <br />
# List of features <br />
Log in and register account
employee can submit request for reimbursement
employee can view accepted, pending, and decline reimbursement
manager can decline or accept reimbursement request
manager can view all users and reimbursement
