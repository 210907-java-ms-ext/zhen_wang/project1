package dev.zhen.data;

import dev.zhen.model.Reimbursement;
import dev.zhen.model.User;

import java.util.ArrayList;

public interface ReimbursementDAO {
    public void createReimbursement(User user, double value);
    public void updateReimbursement(int id);
    public void cancelReimbursement( int id);
    public Reimbursement getReinmbursementById(int id);
    public ArrayList<Reimbursement>getPendingReimbursement(User user);
    public ArrayList<Reimbursement>getAcceptedRequest(User user);
    public ArrayList<Reimbursement>getDeclineReimbursement(User user);
    public ArrayList<Reimbursement>getCurrentPending(User user);
    public ArrayList<Reimbursement>getCurrentAccept(User user);
    public ArrayList<Reimbursement>getCurrentDecline(User user);



}
