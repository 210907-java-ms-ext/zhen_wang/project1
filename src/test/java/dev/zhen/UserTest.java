package dev.zhen;

import dev.zhen.model.Reimbursement;
import dev.zhen.model.User;
import dev.zhen.service.AuthorizationService;
import dev.zhen.service.UserService;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class UserTest {
    private AuthorizationService authorizationService = new AuthorizationService();
    private UserService userService = new UserService();


    @Test


    public void testlogin()
    {
        User user = new User( 1 ,"Zhen" , "Wang", "Zhen" ,"password",  "zhen@email.com" ,"admin");
        User user2 =userService.login("Zhen","password" );

        assertEquals(user, user2);
    }
    @Test
    public void getuserbyid()
    {
        User user = new User( 1 ,"Zhen" , "Wang", "Zhen" ,"password",  "zhen@email.com" ,"admin");
        Object expected = user;
        User user2 = userService.getUserById(1);
        Object actual = user2;
        assertEquals(expected, actual);
    }
    @Test
    public void getReimbursement()
    {
        Reimbursement reimbursement = new Reimbursement( null, 1,"zhenwang",23,2);
        Reimbursement reimbursement1 = userService.getReimburstById(1);
        assertEquals(reimbursement1, reimbursement1);
    }
    @Test
    public void getUserPending()
    {
        User user = new User( 2 ,"Bob" , "test", "bob" ,"cool",  "bob@email.com" ,"user");
        assertNotNull(userService.getCurrentPending(user));


    }
    @Test
    public void getAccept()
    {
        User user = new User( 2 ,"Bob" , "test", "bob" ,"cool",  "bob@email.com" ,"user");
        assertNotNull(userService.getAcceptingReimburstment(user));
    }
    @Test
    public void getAlluser()
    {
        ArrayList<User> alluser = userService.getAllUser();
        int actual = alluser.size();
        int expected = 3;
        assertEquals(actual, expected);
    }
    @Test
    public void getAllPending()
    {
        User user = new User( 1 ,"Zhen" , "Wang", "Zhen" ,"password",  "zhen@email.com" ,"admin");
        assertNotNull(userService.getPendingReimbursement(user));

    }
    @Test
    public void testvalidate()
    {
        boolean actual = authorizationService.validation("1:admin");
        boolean expected = true;
        assertEquals(actual, expected);
    }
}
