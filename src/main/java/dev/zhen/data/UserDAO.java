package dev.zhen.data;

import dev.zhen.model.User;

import java.util.ArrayList;


public interface UserDAO {
    public User logIn(String username , String password);
    public User getUserById(int id);
    public ArrayList<User> getAllUser();
}
