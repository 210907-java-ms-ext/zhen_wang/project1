const token = sessionStorage.getItem("token");

 const xhr = new XMLHttpRequest();
 xhr.onreadystatechange = function(){
    if (xhr.readyState === 4) {
          let userinfo = JSON.parse(xhr.responseText);
          console.log(userinfo);
          member(userinfo);

     }
  }
 xhr.open("GET", "./user");
 xhr.setRequestHeader("Authorization", token);
 xhr.send();

function member(userinfo){
 let employeeTable = document.getElementById("employeeTable");

     for (let user of userinfo) {
         let row = document.createElement("tr");
         let userId = document.createElement("td");
         userId.innerHTML = user.id;
         row.appendChild(userId);
         let fullName = document.createElement("td");
         fullName.innerHTML = user.firstName+" "+user.lastName;
         row.appendChild(fullName);
         let username =document.createElement("td");
         username.innerHTML = user.username;
         row.appendChild(username);
         let email = document.createElement("td");
         email.innerHTML = user.email;
         row.appendChild(email);
         let role = document.createElement("td");
         role.innerHTML = user.role;
         row.appendChild(role);
         employeeTable.appendChild(row);
     }

 }

function pending()
{
  const xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function(){
     if (xhttp.readyState === 4) {
       let pendingInfo = JSON.parse(xhttp.responseText);
       console.log(pendingInfo);
       RTable(pendingInfo);
      }
   }
  xhttp.open("GET", "./reimburst");
  xhttp.setRequestHeader("GetWhat", "pending")
  xhttp.setRequestHeader("Authorization", token);
  xhttp.send();
}
function Decline()
{
  const xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function(){
     if (xhttp.readyState === 4) {
       let acceptedInfo = JSON.parse(xhttp.responseText);
       console.log(acceptedInfo);
       RTable(acceptedInfo);
      }
   }
  xhttp.open("GET", "./reimburst");
  xhttp.setRequestHeader("GetWhat", "decline")
  xhttp.setRequestHeader("Authorization", token);
  xhttp.send();
}
function accepted()
{
  const xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function(){
     if (xhttp.readyState === 4) {
       let acceptedInfo = JSON.parse(xhttp.responseText);
       console.log(acceptedInfo);
       RTable(acceptedInfo);

      }
   }
  xhttp.open("GET", "./reimburst");
  xhttp.setRequestHeader("GetWhat", "accept")
  xhttp.setRequestHeader("Authorization", token);
  xhttp.send();
}

function RTable(pendingInfo){
 let Pending = document.getElementById("ReimburstTable");
 Pending.innerHTML = "";

     for (let pending of pendingInfo) {
         let row = document.createElement("tr");
         let pId = document.createElement("td");
         pId.innerHTML = pending.id;
         row.appendChild(pId);
         let fullName = document.createElement("td");
         fullName.innerHTML = pending.name
         row.appendChild(fullName);
         let value =document.createElement("td");
         value.innerHTML = pending.value;
         row.appendChild(value);
         let isComplete = document.createElement("td");
         console.log(pending.complete);
         if(pending.complete == 2){
         isComplete.innerHTML = "Accepted" ;
         }else if(pending.complete == 1){
         isComplete.innerHTML = "Pending" ;
         }else if(pending.complete == 3)
         {
         isComplete.innerHTML = "Decline";
         }
         row.appendChild(isComplete);
         Pending.appendChild(row);
         }

 }

 function AcceptTheReimburst()
 {
    const reId = document.getElementById("ID").value;
      const xhr = new XMLHttpRequest();
      xhr.open("POST", "./reimburst");

       xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
       const requestBody = `ID=${reId}`;
       console.log(`requestBody: ${requestBody}`);
       xhr.setRequestHeader("Perform", "accept");
       xhr.send(requestBody);

 }
 function DeclineTheReimburst()
 {
    const reId = document.getElementById("ID").value;
     const xhr = new XMLHttpRequest();
     xhr.open("POST", "./reimburst");

      xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      const requestBody = `ID=${reId}`;
      console.log(`requestBody: ${requestBody}`);
      xhr.setRequestHeader("Perform", "decline");
      xhr.send(requestBody);
 }
function exit()
{
    sessionStorage.clear();
    window.location.href="./login.html";
}
