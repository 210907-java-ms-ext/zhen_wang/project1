const token = sessionStorage.getItem("token");

 const xhr = new XMLHttpRequest();
 xhr.onreadystatechange = function(){
    if (xhr.readyState === 4) {
          let userinfo = JSON.parse(xhr.responseText);
          console.log(userinfo);
          Profile(userinfo);

     }
  }
 xhr.open("GET", "./user");
 xhr.setRequestHeader("Authorization", token);
 xhr.send();
function Profile(userinfo) {
    let name = document.getElementById("name")
    name.innerText = userinfo.firstName+" "+userinfo.lastName;
    let username = document.getElementById("username")
    username.innerText = userinfo.username;
    let email = document.getElementById("email")
    email.innerText = userinfo.email;
    let role = document.getElementById("role")
    role.innerText = userinfo.role;
}
function pending()
{
  const xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function(){
     if (xhttp.readyState === 4) {
       let pendingInfo = JSON.parse(xhttp.responseText);
       console.log(pendingInfo);
       RTable(pendingInfo);
      }
   }
  xhttp.open("GET", "./reimburst");
  xhttp.setRequestHeader("GetWhat", "pending")
  xhttp.setRequestHeader("Authorization", token);
  xhttp.send();
}
function Decline()
{
  const xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function(){
     if (xhttp.readyState === 4) {
       let acceptedInfo = JSON.parse(xhttp.responseText);
       console.log(acceptedInfo);
       RTable(acceptedInfo);
      }
   }
  xhttp.open("GET", "./reimburst");
  xhttp.setRequestHeader("GetWhat", "decline")
  xhttp.setRequestHeader("Authorization", token);
  xhttp.send();
}
function accepted()
{
  const xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function(){
     if (xhttp.readyState === 4) {
       let acceptedInfo = JSON.parse(xhttp.responseText);
       console.log(acceptedInfo);
       RTable(acceptedInfo);

      }
   }
  xhttp.open("GET", "./reimburst");
  xhttp.setRequestHeader("GetWhat", "accept")
  xhttp.setRequestHeader("Authorization", token);
  xhttp.send();
}

function RTable(pendingInfo){
 let Pending = document.getElementById("ReimburstTable");
 Pending.innerHTML = "";

     for (let pending of pendingInfo) {
         let row = document.createElement("tr");
         let pId = document.createElement("td");
         pId.innerHTML = pending.id;
         row.appendChild(pId);
         let fullName = document.createElement("td");
         fullName.innerHTML = pending.name
         row.appendChild(fullName);
         let value =document.createElement("td");
         value.innerHTML = pending.value;
         row.appendChild(value);
         let isComplete = document.createElement("td");
         console.log(pending.complete);
         if(pending.complete == 2){
         isComplete.innerHTML = "Accepted" ;
         }else if(pending.complete == 1){
         isComplete.innerHTML = "Pending" ;
         }else if(pending.complete == 3)
         {
         isComplete.innerHTML = "Decline";
         }
         row.appendChild(isComplete);
         Pending.appendChild(row);
         }

 }

 function CreateReimburst()
 {
    const userVal = document.getElementById("EnterVal").value;
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "./reimburst");

    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    const requestBody = `value=${userVal}`;
    console.log(`requestBody: ${requestBody}`);
    xhr.setRequestHeader("Perform", "create");
    xhr.setRequestHeader("Authorization", token);
    xhr.send(requestBody);

 }
function exit()
{
sessionStorage.removeItem("token");
window.location.href="./login.html";
}

