package dev.zhen.data;

import dev.zhen.model.Reimbursement;
import dev.zhen.model.User;
import dev.zhen.service.SqlConnection;

import java.sql.*;
import java.util.ArrayList;

public class ReimbursementDAOImpl implements ReimbursementDAO{
    private SqlConnection sqlConnection = new SqlConnection();

    @Override
    public void createReimbursement(User user, double value) {
        String sql = "insert into reimbursement (user_id, reimburst_name, value, completed) values (?, ? , ?, 1);";

        try (Connection connection = sqlConnection.establishConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, user.getId());
            pstmt.setString(2, user.getFirstName() + user.getLastName());
            pstmt.setDouble(3, value);

            pstmt.executeUpdate();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }

    }

    @Override
    public void updateReimbursement(int id) {
        String sql = "update reimbursement set completed = 2 where reimburst_id = ?";
        try (Connection connection = sqlConnection.establishConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
    }

    public void cancelReimbursement(int id) {
        String sql = "update reimbursement set completed = 3 where reimburst_id = ?";
        try (Connection connection = sqlConnection.establishConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
            ;
        }
    }

    @Override
    public ArrayList<Reimbursement> getAcceptedRequest(User user) {
        ArrayList<Reimbursement> allAcceptReimbursements = new ArrayList<>();
        String sql = "select * from reimbursement where completed = 2";
        try (Connection connection = sqlConnection.establishConnection()) {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reimbursement reimbursement = new Reimbursement();
                reimbursement.setId(rs.getInt("reimburst_id"));
                reimbursement.setValue(rs.getDouble("value"));
                reimbursement.setComplete(rs.getInt("completed"));
                reimbursement.setName(rs.getString("reimburst_name"));
                reimbursement.setUser(user);
                allAcceptReimbursements.add(reimbursement);
            }


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return allAcceptReimbursements;
    }

    @Override
    public ArrayList<Reimbursement> getDeclineReimbursement(User user) {
        ArrayList<Reimbursement> allDeclineReimbursements = new ArrayList<>();
        String sql = "select * from reimbursement where completed = 3";
        try (Connection connection = sqlConnection.establishConnection()) {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reimbursement reimbursement = new Reimbursement();
                reimbursement.setId(rs.getInt("reimburst_id"));
                reimbursement.setValue(rs.getDouble("value"));
                reimbursement.setComplete(rs.getInt("completed"));
                reimbursement.setName(rs.getString("reimburst_name"));
                reimbursement.setUser(user);
                allDeclineReimbursements.add(reimbursement);
            }


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return allDeclineReimbursements;
    }

    @Override
    public Reimbursement getReinmbursementById(int id) {

        String sql = "Select * from reimbursement where reimburst_id = ?";
        try (Connection connection = sqlConnection.establishConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            ResultSet rs = preparedStatement.executeQuery();
            Reimbursement reimbursement = new Reimbursement();
            while (rs.next()) {
                reimbursement.setId(rs.getInt("reimburst_id"));
                reimbursement.setName(rs.getString("reimburst_name"));
                reimbursement.setValue(rs.getDouble("value"));
                reimbursement.setComplete(rs.getInt("completed"));
            }
            return reimbursement;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public ArrayList<Reimbursement> getPendingReimbursement(User user) {
        ArrayList<Reimbursement> allReimbursements = new ArrayList<>();
        String sql = "Select * from reimbursement where completed = 1";
        try (Connection connection = sqlConnection.establishConnection()) {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reimbursement reimbursement = new Reimbursement();
                reimbursement.setId(rs.getInt("reimburst_id"));
                reimbursement.setValue(rs.getDouble("value"));
                reimbursement.setComplete(rs.getInt("completed"));
                reimbursement.setName(rs.getString("reimburst_name"));
                reimbursement.setUser(user);
                allReimbursements.add(reimbursement);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return allReimbursements;
    }

    public ArrayList<Reimbursement> getCurrentPending(User user) {
        ArrayList<Reimbursement> currentPending = new ArrayList<>();
        String sql = "Select * from reimbursement where completed = 1 and user_id = ?";
        try (Connection connection = sqlConnection.establishConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, user.getId());
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                Reimbursement reimbursement = new Reimbursement();
                reimbursement.setId(rs.getInt("reimburst_id"));
                reimbursement.setValue(rs.getDouble("value"));
                reimbursement.setComplete(rs.getInt("completed"));
                reimbursement.setName(rs.getString("reimburst_name"));
                reimbursement.setUser(user);
                currentPending.add(reimbursement);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return currentPending;
    }

    public ArrayList<Reimbursement> getCurrentAccept(User user) {
        ArrayList<Reimbursement> currentAccept = new ArrayList<>();
        String sql = "Select * from reimbursement where completed = 2 and user_id = ?";
        try (Connection connection = sqlConnection.establishConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, user.getId());
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                Reimbursement reimbursement = new Reimbursement();
                reimbursement.setId(rs.getInt("reimburst_id"));
                reimbursement.setValue(rs.getDouble("value"));
                reimbursement.setComplete(rs.getInt("completed"));
                reimbursement.setName(rs.getString("reimburst_name"));
                reimbursement.setUser(user);
                currentAccept.add(reimbursement);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return currentAccept;
    }
    public ArrayList<Reimbursement> getCurrentDecline(User user) {
        ArrayList<Reimbursement> currentAccept = new ArrayList<>();
        String sql = "Select * from reimbursement where completed = 3 and user_id = ?";
        try (Connection connection = sqlConnection.establishConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, user.getId());
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                Reimbursement reimbursement = new Reimbursement();
                reimbursement.setId(rs.getInt("reimburst_id"));
                reimbursement.setValue(rs.getDouble("value"));
                reimbursement.setComplete(rs.getInt("completed"));
                reimbursement.setName(rs.getString("reimburst_name"));
                reimbursement.setUser(user);
                currentAccept.add(reimbursement);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return currentAccept;
    }
}
