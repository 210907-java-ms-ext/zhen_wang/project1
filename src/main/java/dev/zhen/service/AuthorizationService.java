package dev.zhen.service;

import dev.zhen.model.User;

public class AuthorizationService {
    private UserService userService = new UserService();

    public boolean validation(String token) {
        if (token == null) {
            return false;
        }
        String[] tokenArray = token.split(":");
        if (tokenArray.length != 2) {
            return false;
        }
        String id = tokenArray[0];
        if(!id.matches("^\\d+$"))
        {
            return false;
        }
        String role = tokenArray[1];
        if(role.equals("admin")){
            return true;
        }else if (role.equals("user"))
        {
            return true;
        }else{
            return false;
        }
    }
    public User findUserById(String token)
    {
        String[] tokenArray = token.split(":");
        int id = Integer.parseInt(tokenArray[0]);
        return userService.getUserById(id);
    }
}
