package dev.zhen.servlet;

import dev.zhen.model.User;
import dev.zhen.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class AuthorizationServlet extends HttpServlet {
    private UserService userService = new UserService();

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        User user = userService.login(username, password);
        if(user == null)
        {
            response.sendError(401, "Invalid User");
        }else{
            response.setStatus(200);
            String token = user.getId()+":"+user.getRole();
            response.setHeader("Authorization", token);

        }
    }


}
