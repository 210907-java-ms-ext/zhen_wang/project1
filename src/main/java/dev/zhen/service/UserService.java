package dev.zhen.service;

import dev.zhen.data.ReimbursementDAO;
import dev.zhen.data.ReimbursementDAOImpl;
import dev.zhen.data.UserDAO;
import dev.zhen.data.UserDAOImpl;
import dev.zhen.model.Reimbursement;
import dev.zhen.model.User;

import java.util.ArrayList;

public class UserService {
    private User user = new User();
    private UserDAO userdao = new UserDAOImpl();
    private ReimbursementDAO reimburst = new ReimbursementDAOImpl();
    public User login(String username, String password)
    {
        if(username == null || username.isEmpty() || password == null || password.isEmpty())
        {
            return null;
        }
        return userdao.logIn(username, password);
    }
    public User getUserById(int id)
    {
        return userdao.getUserById(id);
    }
    public Reimbursement getReimburstById( int id)
    {
        return reimburst.getReinmbursementById( id);
    }
    public ArrayList<User> getAllUser()
    {

        return userdao.getAllUser();
    }

    public ArrayList<Reimbursement>getPendingReimbursement(User user)
    {

        return reimburst.getPendingReimbursement(user);
    }
    public ArrayList<Reimbursement>getAcceptingReimburstment(User user)
    {
        return reimburst.getAcceptedRequest(user);
    }
    public ArrayList<Reimbursement>getCurrentPending(User user)
    {
        return  reimburst.getCurrentPending(user);
    }
    public ArrayList<Reimbursement>getCurrentAccepting(User user)
    {
        return reimburst.getCurrentAccept(user);
    }
    public void update(int id)
    {
        reimburst.updateReimbursement(id);
    }
    public void cancel(int id)
    {
        reimburst.cancelReimbursement(id);
    }
    public void createReimburst(User user, double value)
    {
        reimburst.createReimbursement(user , value);
    }
    public ArrayList<Reimbursement> getDeclineReimburst(User user)
    {
        return reimburst.getDeclineReimbursement(user);
    }
    public ArrayList<Reimbursement>getCurrentDecline(User user)
    {
        return reimburst.getCurrentDecline(user);
    }
}

