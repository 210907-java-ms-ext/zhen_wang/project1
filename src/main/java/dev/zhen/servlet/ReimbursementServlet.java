package dev.zhen.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.zhen.model.Reimbursement;
import dev.zhen.model.User;
import dev.zhen.service.AuthorizationService;
import dev.zhen.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class ReimbursementServlet extends HttpServlet {

    AuthorizationService authorizationService = new AuthorizationService();
    UserService userService = new UserService();

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String token = request.getHeader("GetWhat");
        String tokenid = request.getHeader("Authorization");
        boolean isValidToken = authorizationService.validation(tokenid);
        if (!isValidToken) {
            response.sendError(400, "Bad Token");
        } else {
            User currentUser = authorizationService.findUserById(tokenid);
            if (currentUser.getId() == 0) {
                response.sendError(401, "no user");
            } else {
                if (currentUser.getRole().equals("admin")) {
                    if (token.equals("pending")) {
                        try (PrintWriter pw = response.getWriter()) {
                            User user = authorizationService.findUserById(tokenid);
                            ArrayList<Reimbursement> pendingReimburse = userService.getPendingReimbursement(user);
                            ObjectMapper om = new ObjectMapper();
                            String userJson = om.writeValueAsString(pendingReimburse);
                            pw.write(userJson);
                        }
                    } else if (token.equals("accept")) {
                        try (PrintWriter pw = response.getWriter()) {
                            User user = authorizationService.findUserById(tokenid);
                            ArrayList<Reimbursement> acceptReimburst = userService.getAcceptingReimburstment(user);
                            ObjectMapper om = new ObjectMapper();
                            String userJson = om.writeValueAsString(acceptReimburst);
                            pw.write(userJson);
                        }
                    }else if (token.equals("decline")) {
                        try (PrintWriter pw = response.getWriter()) {
                            User user = authorizationService.findUserById(tokenid);
                            ArrayList<Reimbursement> acceptReimburst = userService.getDeclineReimburst(user);
                            ObjectMapper om = new ObjectMapper();
                            String userJson = om.writeValueAsString(acceptReimburst);
                            pw.write(userJson);
                        }
                    }
                } else if (currentUser.getRole().equals("user")) {
                    if (token.equals("pending")) {
                        try (PrintWriter pw = response.getWriter()) {
                            User user = authorizationService.findUserById(tokenid);
                            ArrayList<Reimbursement> pendingReimburst = userService.getCurrentPending(user);
                            ObjectMapper om = new ObjectMapper();
                            String userJson = om.writeValueAsString(pendingReimburst);
                            pw.write(userJson);
                        }
                    } else if (token.equals("accept")) {
                        try (PrintWriter pw = response.getWriter()) {
                            User user = authorizationService.findUserById(tokenid);
                            ArrayList<Reimbursement> acceptReimburst = userService.getCurrentAccepting(user);
                            ObjectMapper om = new ObjectMapper();
                            String userJson = om.writeValueAsString(acceptReimburst);
                            pw.write(userJson);
                        }
                    }else if (token.equals("decline")) {
                        try (PrintWriter pw = response.getWriter()) {
                            User user = authorizationService.findUserById(tokenid);
                            ArrayList<Reimbursement> acceptReimburst = userService.getCurrentDecline(user);
                            ObjectMapper om = new ObjectMapper();
                            String userJson = om.writeValueAsString(acceptReimburst);
                            pw.write(userJson);
                        }
                    }
                } else {
                    response.sendError(403, "Wrong user");
                }
            }
        }
    }
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String token = request.getHeader("Perform");
        String id = request.getParameter("ID");
        String us = request.getHeader("Authorization");
        String value = request.getParameter("value");

        if (token.equals("accept")) {
            try {
                int Id = Integer.parseInt(id);
                UserService userService = new UserService();
                Reimbursement checkReimburstment = userService.getReimburstById(Id);
                if(checkReimburstment.getComplete() == 1) {
                    userService.update(Id);
                    response.setStatus(200);
                }else
                {
                    response.sendError(400, "Not Pending");
                }
            } catch (NumberFormatException e) {
                response.sendError(401, "need int");
            }
        }else if(token.equals("decline"))
        {
            try {
                int Id = Integer.parseInt(id);
                userService.cancel(Id);
                response.setStatus(200);
            } catch (NumberFormatException e) {
                response.sendError(401, "need int");
            }
        }else if(token.equals("create"))
        {
            double Val = Double.parseDouble(value);
            User user = authorizationService.findUserById(us);
            userService.createReimburst(user, Val);
        }
    }
}






