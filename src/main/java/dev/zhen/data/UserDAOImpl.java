package dev.zhen.data;

import dev.zhen.model.User;
import dev.zhen.service.SqlConnection;

import java.sql.*;
import java.util.ArrayList;


public class UserDAOImpl implements UserDAO{
    private SqlConnection  sqlConnection = new SqlConnection();

    @Override
    public User logIn(String username, String password) {
        String sql = "Select * from project1 where user_name = ? and user_password = ?";
        try (Connection c = sqlConnection.establishConnection()){
            PreparedStatement pstmt = c.prepareStatement(sql);
            pstmt.setString(1, username);
            pstmt.setString(2, password);
            ResultSet rs = pstmt.executeQuery();

            User user = new User();
            while(rs.next())
            {
                user.setId(rs.getInt("user_id"));
                user.setFirstName(rs.getString("first_name"));
                user.setLastName(rs.getString("last_name"));
                user.setUsername(rs.getString("user_name"));
                user.setPassword(rs.getString("user_password"));
                user.setEmail(rs.getString("user_email"));
                user.setRole(rs.getString("user_role"));
            }

            return user;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User getUserById(int id) {
        String sql = "Select * from project1 where user_id = ?";
        try (Connection c = sqlConnection.establishConnection()) {
            PreparedStatement pstmt = c.prepareStatement(sql);
            pstmt.setInt(1, id);
            ResultSet rs = pstmt.executeQuery();

            User user = new User();
            while (rs.next()) {
                user.setId(rs.getInt("user_id"));
                user.setFirstName(rs.getString("first_name"));
                user.setLastName(rs.getString("last_name"));
                user.setUsername(rs.getString("user_name"));
                user.setPassword(rs.getString("user_password"));
                user.setEmail(rs.getString("user_email"));
                user.setRole(rs.getString("user_role"));
            }
            return user;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ArrayList<User> getAllUser() {
        ArrayList<User> allUser = new ArrayList<>();
        String sql = "Select * from project1";
        try (Connection c = sqlConnection.establishConnection())
        {
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next())
            {
                User user = new User();
                user.setId(rs.getInt("user_id"));
                user.setFirstName(rs.getString("first_name"));
                user.setLastName(rs.getString("last_name"));
                user.setUsername(rs.getString("user_name"));
                user.setPassword(rs.getString("user_password"));
                user.setEmail(rs.getString("user_email"));
                user.setRole(rs.getString("user_role"));
                allUser.add(user);
            }
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
        return allUser;
    }
}
